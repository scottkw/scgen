# Scale &amp; Chord Generator

<h3>Description:</h3>
Scale & Chord Generator displays the notes of a selected chord or scale on a musical staff, piano keyboard, guitar fretboard or bass fretboard. In addition, it will display the name of a chord or scale that can be played in accompaniment with the original selection. 

<h3>Interface</h3>

<img src="https://raw.githubusercontent.com/scottkw/scgen/master/mainWindow.jpg">

<ol>
<li>Menus	As with most programs the menus allow the user to perform tasks aside from the normal tasks allowed by the various elements of the GUI. The SCG menus are as follows: 
  <ul>
    <li>File</li>
 	    <ul>
        <li>Rebuild Database - Rebuilds the app's database in the event of a corruption</li>
 	      <li>Exit - Exits the app</li>
      </ul>
    <li>Help</li>
 	    <ul>
        <li>Help - Displays the help window</li>
 	      <li>About - Displays the about dialog box</li>
      </ul>
  </ul>
<li>Display Type	Determines what type of graphical display is used. As soon as a selection is made, the display will change.
<li>Active Display	Determines whether the current chord or scale is displayed.
<li>Display	The graphical representation of the current chord or scale is displayed here.
<li>Selection Box Labels	The chord selection box label and the scale selection box label will be changed to all caps according to which one has been selected.
<li>Chord Selection Box	The list of all available chords. When a chord is selected, it's relative scale will be displayed in the scale selection box.
<li>Scale Selection Box	The list of all available scales. When a scale is selected, it's relative chord will be displayed in the chord selection box.
<li>Key Selection Box	List of all keys.
<li>Notes List	Displays the list of all notes in the current chord or scale.
</ol>

<h3>Release Notes \ Change Log:</h3>

<p>
Version: 3.0<br/>
Release date: 2005-04-14<br/>
OS: Windows, Linux, MacOS X (just about anything that will run Python)<br/>
Development platform: Python, wxPython and PythonCard<br/>
Notes: The latest version of SCG. I wrote it because I love python, wanted something to make my first official open source release and wanted the app to be multi-platform.<br/>
</p>

<p>
Version: 2.0<br/>
Release date: 1992<br/>
OS: OS/2<br/>
Development platform: VisualREXX<br/>
Notes: The second version of SCG. I wrote it because I wanted a GUI version of SCG, wanted to learn REXX and had just bought OS/2 because I was fed up with Windows 3.1.<br/>
</p>

<p>
Version: 1.0<br/>
Release date: 1990<br/>
OS: MS-DOS<br/>
Development platform: MS QuickBasic<br/>
Notes: The first version of SCG. I wrote it because I was a poor CS major that couldn't afford a commercial product and FOSS software was still a pipedream.<br/>
</p>
