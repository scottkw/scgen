
@echo off

echo.
echo Building scgen...
echo.

python setup.py py2exe --excludes=Image,Numeric

echo.
echo Build complete.
echo.