#!/usr/bin/python

# Import all needed modules
from PythonCard import dialog, model, graphic
from PythonCard.components import textfield, staticbox, statictext, combobox, radiogroup, bitmapcanvas, htmlwindow
import apsw, os, sys, wx

rsrcMain = 	{'application':
			{	'type':'Application',
				'name':'appMain',
				'backgrounds': [
					{	'type':'Background',
						'name':'backgroundMainWindow',
						'title':'Scale & Chord Generator',
						'icon':'scgen.ico',
						'size':(434, 431),

						'menubar': 
							{	'type':'MenuBar',
								'menus': [
									{	'type':'Menu',
										'name':'menuFile',
										'label':'&File',
										'items': [
											{	'type':'MenuItem',
												'name':'menuFileRebuildDB',
												'label':'&Rebuild Database',
											},
											{	'type':'MenuItem',
												'name':'menuFileExit',
												'label':'E&xit',
												'command':'exit',
											},
										]
									},
									{	'type':'Menu',
										'name':'menuHelp',
										'label':'&Help',
										'items': [
											{	'type':'MenuItem',
												'name':'menuHelpHelp',
												'label':'&Help',
											},
											{	'type':'MenuItem',
												'name':'menuHelpAbout',
												'label':'&About',
											},
										]
									},
								]
							},
						'components': [
							{	'type':'TextField', 
								'name':'textFieldNotes', 
								'position':(141, 127), 
								'size':(262, -1), 
								'editable':False, 
								'toolTip':'All the notes in the selected chord/scale', 
							},
							{	'type':'StaticBox', 
								'name':'staticBoxNotes', 
								'position':(130, 110), 
								'size':(287, 47), 
								'label':'Notes', 
							},
							{	'type':'StaticBox', 
								'name':'staticBoxDetails', 
								'position':(108, 5), 
								'size':(309, 102), 
								'label':'Details', 
							},
							{	'type':'StaticBox', 
								'name':'staticBoxDisplay', 
								'position':(10, 163), 
								'size':(407, 205), 
								'label':'Display', 
							},
							{	'type':'BitmapCanvas', 
								'name':'bitmapCanvasDisplay', 
								'position':(18, 183), 
								'size':(389, 177), 
								'backgroundColor':(255, 255, 255), 
								'toolTip':'Display area', 
							},
							{	'type':'RadioGroup', 
								'name':'radioGroupDisplayType', 
								'position':(10, 5), 
								'size':(89, 102), 
								'items':['Staff', 'Piano', 'Guitar', 'Bass'], 
								'label':'Display Type', 
								'layout':'vertical', 
								'max':1, 
								'stringSelection':'Staff', 
								'toolTip':'Determines which type of display is active', 
							},
							{	'type':'StaticText', 
								'name':'staticTextKey', 
								'position':(120, 77), 
								'size':(36, -1), 
								'text':'Key:', 
							},
							{	'type':'StaticText', 
								'name':'staticTextScale', 
								'position':(120, 52), 
								'text':'Scale:', 
							},
							{	'type':'StaticText', 
								'name':'staticTextChord', 
								'position':(120, 27), 
								'text':'Chord:', 
							},
							{	'type':'ComboBox', 
								'name':'comboBoxKeys', 
								'position':(175, 72), 
								'size':(230, -1), 
								'items':[], 
								'toolTip':'Active key', 
							},
							{	'type':'ComboBox', 
								'name':'comboBoxScales', 
								'position':(175, 47), 
								'size':(230, -1), 
								'items':[], 
								'toolTip':'Name of the scale', 
							},
							{	'type':'ComboBox', 
								'name':'comboBoxChords', 
								'position':(175, 22), 
								'size':(230, -1), 
								'items':[], 
								'toolTip':'Name of the chord', 
							},
							{	'type':'RadioGroup', 
								'name':'radioGroupActiveDisplay', 
								'position':(10, 110), 
								'size':(111, 47), 
								'items':['Chord', 'Scale'], 
								'label':'Active Display', 
								'layout':'horizontal', 
								'max':1, 
								'stringSelection':'Scale', 
								'toolTip':'Determines which item is displayed in the graphic dispay', 
							},
						] # end components
					} # end background
				] # end backgrounds
			}
		}

rsrcHelp = 	{'application':
				{'type':'Application',
						'name':'appHelp',
						'backgrounds': [
							{'type':'Background',
									'name':'backgroundHelp',
									'title':'Scale & Chord Generator - Help',
									'size':(330, 350),
									'style':['resizeable'],
									'components': [
										{'type':'HtmlWindow', 
												'name':'htmlWindowDisplay', 
												'position':(10, 10), 
												'size':(300, 300), 
												'backgroundColor':(255, 255, 255), 
												'text':'scgenHelp.html'
										},
									] # end components
							} # end background
						] # end backgrounds
				}
			}

# Class definition for main window
class scgenMain(model.Background):

	#Define all class variables
	Chords = []
	Scales = []
	Keys = []
	
	def rebuild_Database(self):
		# Delete the current database
		try:
			os.remove("scgen.db")
		except:
			pass

		# Create database
		connection=apsw.Connection("scgen.db")
		cursor=connection.cursor()

		# Create tables
		cursor.execute("create table chords('chord','pattern','scale')")
		cursor.execute("create table scales('scale','pattern','chord')")

		# Populate chords table
		cursor.execute("insert into chords values('Major Triad','43','Lydian Mode')")
		cursor.execute("insert into chords values('(add 9)','223','Lydian Mode')")
		cursor.execute("insert into chords values('SUS','52','Minor Pentatonic')")
		cursor.execute("insert into chords values('(b5)','42','Minor Pentatonic')")
		cursor.execute("insert into chords values('+ (augmented)','44','+ (augmented)')")
		cursor.execute("insert into chords values('6','432','Lydian Mode')")
		cursor.execute("insert into chords values('M7(b5)','425','Minor Pentatonic')")
		cursor.execute("insert into chords values('M9(#11)','43434','Minor Pentatonic')")
		cursor.execute("insert into chords values('M7','434','Major Scale')")
		cursor.execute("insert into chords values('M9','4343','Major Scale')")
		cursor.execute("insert into chords values('M13','43437','Major Scale')")
		cursor.execute("insert into chords values('M7+','443','+ (augmented)')")
		cursor.execute("insert into chords values('M9+','4433','+ (augmented)')")
		cursor.execute("insert into chords values('M13(#11)','434343','Minor Pentatonic')")
		cursor.execute("insert into chords values('Minor Triad','34','Aeolian Mode')")
		cursor.execute("insert into chords values('(add 9)','214','Aeolian Mode')")
		cursor.execute("insert into chords values('m6','342','Asc. Melodic Minor')")
		cursor.execute("insert into chords values('o7 (diminished)','333','Lydian Diminished')")
		cursor.execute("insert into chords values('m7(b5)','334','Locrian #2')")
		cursor.execute("insert into chords values('m7','343','Aeolian Mode')")
		cursor.execute("insert into chords values('m9','3434','Aeolian Mode')")
		cursor.execute("insert into chords values('m11','34343','Aeolian Mode')")
		cursor.execute("insert into chords values('m13','343434','Aeolian Mode')")
		cursor.execute("insert into chords values('m(M7)','344','Harmonic Minor')")
		cursor.execute("insert into chords values('m9(M7)','3443','Harmonic Minor')")
		cursor.execute("insert into chords values('7','433','Lydian b7')")
		cursor.execute("insert into chords values('7sus','523','Lydian b7')")
		cursor.execute("insert into chords values('9','4334','Lydian b7')")
		cursor.execute("insert into chords values('9sus','5234','Lydian b7')")
		cursor.execute("insert into chords values('11','7343','Lydian b7')")
		cursor.execute("insert into chords values('13','43347','Lydian b7')")
		cursor.execute("insert into chords values('IV/V (Ex. F/G)','2554','Major Pentatonic')")
		cursor.execute("insert into chords values('7(b5)','424','Whole Tone')")
		cursor.execute("insert into chords values('9(b5)','4244','Whole Tone')")
		cursor.execute("insert into chords values('9(#11)','43344','Whole Tone')")
		cursor.execute("insert into chords values('13(#11)','433443','Whole Tone')")
		cursor.execute("insert into chords values('7(b9)','4333','Half-Whole Diminished')")
		cursor.execute("insert into chords values('11(b9)','43334','Half-Whole Diminished')")
		cursor.execute("insert into chords values('13(b9)','43338','Half-Whole Diminished')")
		cursor.execute("insert into chords values('7(#9)','4335','Blues Scale')")
		cursor.execute("insert into chords values('13(#9)','43356','Blues Scale')")
		cursor.execute("insert into chords values('7(b9#9)','43332','Half-Whole Diminished')")
		cursor.execute("insert into chords values('7(b9b5)','4243','Super Locrian')")
		cursor.execute("insert into chords values('7+','442','Whole Tone')")
		cursor.execute("insert into chords values('9+','4424','Whole Tone')")
		cursor.execute("insert into chords values('7+(#9)','4425','Super Locrian')")
		cursor.execute("insert into chords values('7+(b9)','4423','Super Locrian')")
		cursor.execute("insert into chords values('13(b9#11)','433353','Super Locrian')")

		# Populate the scales table
		cursor.execute("insert into scales values('Ionian Mode','2212221','Major Triad')")
		cursor.execute("insert into scales values('Dorian Mode','2122212','m7')")
		cursor.execute("insert into scales values('Phrygian Mode ','1222122','m7')")
		cursor.execute("insert into scales values('Lydian Mode','2221221','M7')")
		cursor.execute("insert into scales values('Mixolydian','2212212','o7 (diminished)')")
		cursor.execute("insert into scales values('Aeolian Mode','2122122','m7')")
		cursor.execute("insert into scales values('Locrian Mode','1221222','m7(b5)')")
		cursor.execute("insert into scales values('Major Scale','2212221','Major Triad')")
		cursor.execute("insert into scales values('Harmonic Minor','2122131','m(M7)')")
		cursor.execute("insert into scales values('Asc. Melodic Minor','2122221','m(M7)')")
		cursor.execute("insert into scales values('Des. Melodic Minor','2122122','m7')")
		cursor.execute("insert into scales values('Natural Minor','2122122','m7')")
		cursor.execute("insert into scales values('Lydian Diminished','2131221','o7 (diminished)')")
		cursor.execute("insert into scales values('Lydian Augmented','2222121','M7+')")
		cursor.execute("insert into scales values('Lydian b7','2221212','7(b5)')")
		cursor.execute("insert into scales values('Locrian #2','2121222','m7(b5)')")
		cursor.execute("insert into scales values('Super Locrian','1212222','7(b9b5)')")
		cursor.execute("insert into scales values('Whole Tone','2222222','7(b5)')")
		cursor.execute("insert into scales values('Whole-Half Diminished','21212121','m7(b5)')")
		cursor.execute("insert into scales values('Half-Whole Diminished','12121212','7(#9)')")
		cursor.execute("insert into scales values('Augmented','313131','M7+')")
		cursor.execute("insert into scales values('Hindu','2212131','M7+')")
		cursor.execute("insert into scales values('Major Pentatonic','22323','IV/V (Ex. F/G)')")
		cursor.execute("insert into scales values('Minor Pentatonic','32232','m7')")
		cursor.execute("insert into scales values('Blues Scale','32232','m7')")
		cursor.execute("insert into scales values('Major Triad','2212221','Major Triad')")
		cursor.execute("insert into scales values('(add 9)','2212221','(add 9)')")
		cursor.execute("insert into scales values('SUS','2212221','SUS')")
		cursor.execute("insert into scales values('(b5)','2211321','(b5)')")
		cursor.execute("insert into scales values('+ (augmented)','2213121','+ (augmented)')")
		cursor.execute("insert into scales values('6','2212221','6')")
		cursor.execute("insert into scales values('M7(b5),M9(#11)','2221221','M7(b5)')")
		cursor.execute("insert into scales values('M7,9,13','2221221','M7')")
		cursor.execute("insert into scales values('M7+,M9+','2213121','M7+')")
		cursor.execute("insert into scales values('M13(#11)','2221221','M13(#11)')")
		cursor.execute("insert into scales values('Minor Triad','2122212','Minor Triad')")
		cursor.execute("insert into scales values('(add 9)','2122212','(add 9)')")
		cursor.execute("insert into scales values('m6','2122212','m6')")
		cursor.execute("insert into scales values('o7 (diminished)','2121212','o7 (diminished)')")
		cursor.execute("insert into scales values('m7(b5)','1221222','m7(b5)')")
		cursor.execute("insert into scales values('m7,9,11,13','2122212','m7')")
		cursor.execute("insert into scales values('m(M7),m9(M7)','2122221','m(M7)')")
		cursor.execute("insert into scales values('7,7sus,9,11,13','2212212','7')")
		cursor.execute("insert into scales values('IV/V (Ex. F/G)','2221221','IV/V (Ex. F/G)')")
		cursor.execute("insert into scales values('7(b5),9(#11),13(#11)','2221212','7(b5)')")
		cursor.execute("insert into scales values('7(b9),11(b9),13(b9)','1312212','7(b9)')")
		cursor.execute("insert into scales values('7(#9),13(#9)','3112212','7(#9)')")
		cursor.execute("insert into scales values('7(b9#9)','1222212','7(b9#9)')")
		cursor.execute("insert into scales values('7(b9b5),7(b9#11)','1311312','7(b9b5)')")
		cursor.execute("insert into scales values('7+,9+','2213112','7+')")
		cursor.execute("insert into scales values('7+(#9)','1313112','7+(#9)')")
		cursor.execute("insert into scales values('7+(b9)','1313112','7+(b9)')")
		cursor.execute("insert into scales values('13(b9#11)','1321212','13(b9#11)')")
	
	def on_initialize(self, event):

		try:
			# Open the scgen database
			connection=apsw.Connection("scgen.db")
			cursor=connection.cursor()

			# Populate the chords list from the database
			for chord,pattern,scale in cursor.execute("select chord,pattern,scale from chords"):
				self.Chords.append([chord,pattern,scale])
			
			# Populate the scales list from the database
			for scale,pattern,chord in cursor.execute("select scale,pattern,chord from scales"):
				self.Scales.append([scale,pattern,chord])
		except:
			result = dialog.messageDialog(self, 'There appears to be a problem with the database.\nWould you like to begin the database rebuild routine?', 'Rebuild Database', wx.ICON_QUESTION | wx.YES_NO)
			if result.accepted:
				self.rebuild_Database()
				dialog.messageDialog(self, 'Database rebuild complete.\nPlease restart the application.', 'Rebuild Database', wx.ICON_INFORMATION | wx.OK)
			sys.exit()

		# Populate the keys list
		self.Keys = ["A", "A#/Bb", "B", "C", "C#/Db", "D", "D#/Eb", "E", "F", "F#/Gb", "G", "G#/Ab"]
  
		# Populate the chords, scales and keys combo boxes from the lists initiated above
		self.components.comboBoxChords.items = [list[0] for list in self.Chords]
		self.components.comboBoxScales.items = [list[0] for list in self.Scales]
		self.components.comboBoxKeys.items = self.Keys
		
		# Set the default selection for the chords, scales and keys combo boxes
		self.components.comboBoxChords.stringSelection = self.Chords[0][0]
		self.components.comboBoxKeys.stringSelection = self.Keys[0]
		for sublist in self.Chords:
			if sublist[0] == self.components.comboBoxChords.text:
				self.components.comboBoxScales.stringSelection = sublist[2]
		
		# Set the proper text for the chord and scales combo box labels
		self.components.staticTextChord.text = 'CHORD:'
		self.components.staticTextScale.text = 'Scale:'
		
		# Update the display (notes listing and graphical display) and refresh the window
		self.update_Display()
		self.Refresh()
		
	def on_menuFileRebuildDB_select(self, event):
		result = dialog.messageDialog(self, 'Would you like to begin the database rebuild routine?', 'Rebuild Database', wx.ICON_QUESTION | wx.YES_NO)
		if result.accepted:
			self.rebuild_Database()
			dialog.messageDialog(self, 'Database rebuild complete.', 'Rebuild Database', wx.ICON_INFORMATION | wx.OK)
	
	def on_menuHelpHelp_select(self, event):	
		self.helpWindow = model.childWindow(self, scgenHelp, None, rsrcHelp)
		self.helpWindow.visible = True

	def on_menuHelpAbout_select(self, event):
		dialog.scrolledMessageDialog(self, \
			'\nScale & Chord Generator\n' + \
			'Version 3.0\n' + \
			'Released 2005-04-14\n' + \
			'Written by Ken Scott\n\n' + \
			'http://scgen.sourceforge.net\n\n' + \
			'Scale & Chord Generator gives you a visual representation of a selected chord\scale in any key and it\'s corresponding scale\chord ' + \
			'on a staff, piano keyboard, guitar fretboard or bass guitar fretboard.\n\n' + \
			'Release Notes \ Change Log:\n===========================\n\n' + \
			'Version: 3.0\n\n' + \
			'Release date: 2005-04-14\n\n' + \
			'OS: Windows, Linux, MacOS X (just about anything that will run Python)\n\n' + \
			'Development platform: Python, wxPython and PythonCard\n\n' + \
			'Notes: The latest version of SCG.  I wrote it because I love python, wanted something to make my first official open source release ' + \
			'and wanted the app to be multi-platform.\n' + \
			'\n----------\n\n' \
			'Version: 2.0\n\n' + \
			'Release date: 1992\n\n' + \
			'OS: OS/2\n\n' + \
			'Development platform: VisualREXX\n\n' + \
			'Notes: The second version of SCG.  I wrote it because I wanted a GUI version of SCG, wanted to learn REXX and had just bought OS/2 ' + \
			'because I was fed up with Windows 3.1.\n' + \
			'\n----------\n\n' \
			'Version: 1.0\n\n' + \
			'Release date: 1990\n\n' + \
			'OS: MS-DOS\n\n' + \
			'Development platform: MS QuickBasic\n\n' + \
			'Notes: The first version of SCG.  I wrote it because I was a poor CS major that couldn\'t afford a commercial ' + \
			'product and FOSS software was still a pipedream.\n' + \
			'\n----------\n\n' \
			, 'About Scale & Chord Generator')
		
	def on_comboBoxChords_select(self, event):
		
		# Set the chord and scale combo box labels to indicate that a chord has been selected
		self.components.staticTextChord.text = 'CHORD:'
		self.components.staticTextScale.text = 'Scale:'
		
		# Set the scale that is relative to the selected chord
		for sublist in self.Chords:
			if sublist[0] == self.components.comboBoxChords.stringSelection:
				self.components.comboBoxScales.stringSelection = sublist[2]
		
		# Update the display (notes listing and graphical display) and refresh the window
		self.update_Display()
		self.Refresh()

	def on_comboBoxScales_select(self, event):
		
		# Set the chord and scale combo box labels to indicate that a scale has been selected
		self.components.staticTextChord.text = 'Chord:'
		self.components.staticTextScale.text = 'SCALE:'
		
		# Set the chord that is relative to the selected scale
		for sublist in self.Scales:
			if sublist[0] == self.components.comboBoxScales.stringSelection:
				self.components.comboBoxChords.stringSelection = sublist[2]
		
		# Update the display (notes listing and graphical display) and refresh the window
		self.update_Display()
		self.Refresh()
	
	def on_radioGroupActiveDisplay_select(self, event):
		
		# Update the display (notes listing and graphical display) and refresh the window
		self.update_Display()
		self.Refresh()

	def on_radioGroupDisplayType_select(self, event):
		
		# Update the display (notes listing and graphical display) and refresh the window
		self.update_Display()
		self.Refresh()

	def on_comboBoxKeys_select(self, event):
		
		# Update the display (notes listing and graphical display) and refresh the window
		self.update_Display()
		self.Refresh()
		
	def update_Display(self):
		
		# Determine which pattern to use
		if self.components.radioGroupActiveDisplay.stringSelection == "Chord":
			for sublist in self.Chords:
				if sublist[0] == self.components.comboBoxChords.stringSelection:
					Pattern = sublist[1]
		else:
			for sublist in self.Scales:
				if sublist[0] == self.components.comboBoxScales.stringSelection:
					Pattern = sublist[1]

		# Update the Notes field
		Notes = self.components.comboBoxKeys.stringSelection + ", "
		CurrentNote = self.Keys.index(self.components.comboBoxKeys.stringSelection)
		for i in range(0, len(Pattern)):
			Notes = Notes + self.Keys[(CurrentNote + int(Pattern[i])) % 12] + ", "
			CurrentNote = CurrentNote + int(Pattern[i])
		Notes = Notes[0:-2]
		self.components.textFieldNotes.text = Notes
		
		#Update the graphic display
		self.components.bitmapCanvasDisplay.clear()
		if self.components.radioGroupDisplayType.stringSelection == "Staff":
			self.paint_Staff(Pattern)
		elif self.components.radioGroupDisplayType.stringSelection == "Piano":
			self.paint_Piano(Pattern)
		elif self.components.radioGroupDisplayType.stringSelection == "Guitar":
			self.paint_Guitar(Pattern)
		elif self.components.radioGroupDisplayType.stringSelection == "Bass":
			self.paint_Bass(Pattern)

	def paint_Staff(self, Pattern):
		
		# Establish the y-coordinates for each note on the treble and bass cleffs
		Treble = [56, 56, 51, 46, 46, 40, 40, 72, 67, 67, 61, 61]
		Bass = [119, 151, 145, 140, 140, 135, 135, 130, 124, 124, 119, 119]
		
		# Establish the beginning x-cooridnate, offset and size of the notes 
		StartingXPoint = 75
		NoteSize = 5
		StemHeight = 25
		if self.components.radioGroupActiveDisplay.stringSelection == "Scale":
			Offset = 20
		else:
			Offset = 0
		
		# Place the staff bitmap on the canvas
		StaffBitmap = graphic.Bitmap("Staff.bmp")
		self.components.bitmapCanvasDisplay.drawBitmap(StaffBitmap, [0,0])
		
		# Set foreground and fill color to black
		self.components.bitmapCanvasDisplay.foregroundColor = 'black'
		self.components.bitmapCanvasDisplay.fillColor = 'black'
		
		# Iterate through each note and display it on the treble and bass cleffs
		CurrentXPosition = StartingXPoint

		# Determine the selected key and mark it
		root = self.Keys.index(self.components.comboBoxKeys.stringSelection)

		# Determine if the root note is sharped
		if self.Keys[root].count("#") != 0:
			# If the active display is a scale then draw the sharp at the current x-coordinate, y-coordinate - 8
			# and set the current x-coordinate 15 pixels to the right
			if Offset != 0:
				self.components.bitmapCanvasDisplay.drawText("#", [CurrentXPosition, Treble[root] - 8])
				self.components.bitmapCanvasDisplay.drawText("#", [CurrentXPosition, Bass[root] - 8])
				CurrentXPosition = CurrentXPosition + 15
			# Otherwise draw the sharp 15 pixels to the left and 8 pixels up
			else:
				self.components.bitmapCanvasDisplay.drawText("#", [CurrentXPosition - 15, Treble[root] - 8])
				self.components.bitmapCanvasDisplay.drawText("#", [CurrentXPosition - 15, Bass[root] - 8])

		# Draw the treble and bass notes and their stems then change the current x-coordinate to the right by the offset
		self.components.bitmapCanvasDisplay.drawCircle([CurrentXPosition, Treble[root]],NoteSize)
		self.components.bitmapCanvasDisplay.drawLine([CurrentXPosition + 4, Treble[root]], [CurrentXPosition + 4, Treble[root]  - StemHeight])
		self.components.bitmapCanvasDisplay.drawCircle([CurrentXPosition, Bass[root]],NoteSize)
		self.components.bitmapCanvasDisplay.drawLine([CurrentXPosition - 5, Bass[root]], [CurrentXPosition - 5, Bass[root] + StemHeight])
		CurrentXPosition = CurrentXPosition + Offset
		
		# Mark each note in the pattern beyond the root
		for digit in Pattern:
			#Determine which note to draw
			WhichNote = (root + int(digit)) % 12
			
			# Determine if the root note is sharped
			if self.Keys[WhichNote].count("#") != 0:
				# If the active display is a scale then draw the sharp at the current x-coordinate, y-coordinate - 8
				# and set the current x-coordinate 15 pixels to the right
				if Offset != 0:
					self.components.bitmapCanvasDisplay.drawText("#", [CurrentXPosition, Treble[WhichNote] - 8])
					self.components.bitmapCanvasDisplay.drawText("#", [CurrentXPosition, Bass[WhichNote] - 8])
					CurrentXPosition = CurrentXPosition + 15
				# Otherwise draw the sharp 15 pixels to the left and 8 pixels up
				else:
					self.components.bitmapCanvasDisplay.drawText("#", [CurrentXPosition - 15, Treble[WhichNote] - 8])
					self.components.bitmapCanvasDisplay.drawText("#", [CurrentXPosition - 15, Bass[WhichNote] - 8])
				
			# Draw the treble and bass notes and their stems then change the current x-coordinate to the right by the offset
			self.components.bitmapCanvasDisplay.drawCircle([CurrentXPosition, Treble[WhichNote]],NoteSize)
			self.components.bitmapCanvasDisplay.drawLine([CurrentXPosition + 4, Treble[WhichNote]], [CurrentXPosition + 4, Treble[WhichNote] - StemHeight])
			self.components.bitmapCanvasDisplay.drawCircle([CurrentXPosition, Bass[WhichNote]],NoteSize)
			self.components.bitmapCanvasDisplay.drawLine([CurrentXPosition - 5, Bass[WhichNote]], [CurrentXPosition - 5, Bass[WhichNote] + StemHeight])
			root = root + int(digit)
			CurrentXPosition = CurrentXPosition + Offset

	def paint_Piano(self, Pattern):

		# Establish points for each note marker on the keyboard
		# Each line pertains to a given note, listed in alphabetical order
		NoteMarkers = [ [296,145], \
		                [322,100], \
						[348,145], \
						[36,145], \
						[62,100], \
						[88,145], \
						[114,100], \
						[140,145], \
						[192,145], \
						[218,100], \
						[244,145], \
						[270,100] ]

		# Set proper colors and draw the outer border of the keyboard
		self.components.bitmapCanvasDisplay.fillColor = "white"
		self.components.bitmapCanvasDisplay.foregroundColor = "black"
		self.components.bitmapCanvasDisplay.drawRectangle([10,10],[364,155])
		
		# Set proper colors and draw each key (vertical lines and filled boxes for black keys)
		self.components.bitmapCanvasDisplay.fillColor = "black"
		for i in range(62, 374):
			if (i - 10) % 52 == 0:
				self.components.bitmapCanvasDisplay.drawLine([i,10],[i,165])
				# Draw a black key
				if i != 166:
					self.components.bitmapCanvasDisplay.drawRectangle([i - 20,10],[40,110])
		
		# Change to red for the note markers
		self.components.bitmapCanvasDisplay.fillColor = "red"
		self.components.bitmapCanvasDisplay.foregroundColor = "red"
		
		# Determine the selected key and mark it
		root = self.Keys.index(self.components.comboBoxKeys.stringSelection)
		self.components.bitmapCanvasDisplay.drawCircle(NoteMarkers[root],5)
		
		# Mark each note in the pattern beyond the root
		for digit in Pattern:
			self.components.bitmapCanvasDisplay.drawCircle(NoteMarkers[(root + int(digit)) % 12],5)
			root = root + int(digit)

	def paint_Guitar(self, Pattern):
		
		# Establish points for each note marker on the fretboard
		# Each note may have multiple locations
		# Each line pertains to a given note, listed in alphabetical order
		NoteMarkers = [ [[10,134],[146,72]], \
		                [[55,134],[237,72]], \
						[[146,134],[328,72],[10,41]], \
						[[237,134],[55,41]], \
						[[328,134],[146,41]], \
						[[10,103],[237,41]], \
						[[55,103],[328,41]], \
						[[10,165],[146,103],[10,10]], \
						[[55,165],[237,103],[55,10]], \
						[[146,165],[328,103],[146,10]], \
						[[237,165],[10,72],[237,10]], \
						[[328,165],[55,72],[328,10]] ]

		# Set proper colors to draw the fretboard
		self.components.bitmapCanvasDisplay.fillColor = "white"
		self.components.bitmapCanvasDisplay.foregroundColor = "black"
		
		# Draw outer border of the fretboard
		self.components.bitmapCanvasDisplay.drawRectangle([10,10],[364,155])
		
		# Draw the "strings" (horizontal lines) on the fretboard
		for i in range(10,165):
			if (i - 10) % 31 == 0:
				self.components.bitmapCanvasDisplay.drawLine([10,i],[374,i])
		
		# Draw the "frets" (vertical lines) on the fretboard
		for i in range(101,374):
			if (i - 10) % 91 == 0:
				self.components.bitmapCanvasDisplay.drawLine([i,10],[i,165])
		
		# If the active display is scale...
		if self.components.radioGroupActiveDisplay.stringSelection == "Scale":
			self.mark_Fretboard_Scale(Pattern, NoteMarkers)
		# The active display is chord...
		else:
			# Create a list containing the root note and vertical position for each string
			Strings = [["E",165], ["A",134], ["D",103], ["G",72], ["B",41], ["E",10]]
			self.mark_Fretboard_Chord(Strings, NoteMarkers)
			
	def paint_Bass(self, Pattern):

		# Establish points for each note marker on the fretboard
		# Each note may have multiple locations
		# Each line pertains to a given note, listed in alphabetical order
		NoteMarkers = [ [[10,112],[146,10]], \
		                [[55,112],[237,10]], \
						[[146,112],[328,10]], \
						[[237,112]], \
						[[328,112]], \
						[[10,61]], \
						[[55,61]], \
						[[10,163],[146,61]], \
						[[55,163],[237,61]], \
						[[146,163],[328,61]], \
						[[237,163],[10,10]], \
						[[328,163],[55,10]] ]

		# Set proper colors to draw the fretboard
		self.components.bitmapCanvasDisplay.fillColor = "white"
		self.components.bitmapCanvasDisplay.foregroundColor = "black"
		
		# Draw the outer border and "strings" (horizontal lines) of the fretboard
		self.components.bitmapCanvasDisplay.drawRectangle([10,10],[364,153])
		self.components.bitmapCanvasDisplay.drawLine([10,61],[374,61])
		self.components.bitmapCanvasDisplay.drawLine([10,112],[374,112])
		
		# Draw the "frets" (vertical lines) on the fretboard
		for i in range(101,374):
			if (i - 10) % 91 == 0:
				self.components.bitmapCanvasDisplay.drawLine([i,10],[i,163])
		
		# If the active display is scale...
		if self.components.radioGroupActiveDisplay.stringSelection == "Scale":
			self.mark_Fretboard_Scale(Pattern, NoteMarkers)
		# The active display is chord...
		else:
			# Create a list containing the root note and vertical position for each string
			Strings = [["E",163], ["A",112], ["D",61], ["G",10]]
			self.mark_Fretboard_Chord(Strings, NoteMarkers)
	
	def mark_Fretboard_Scale(self, Pattern, NoteMarkers):
	
		# Change to red for the note markers
		self.components.bitmapCanvasDisplay.foregroundColor = "red"
		
		# Determine the selected key
		root = self.Keys.index(self.components.comboBoxKeys.stringSelection)

		# Mark the root note
		for element in NoteMarkers[root]:
			# If the note is an open string note, draw a red circle filled in white
			# Otherwise, draw a red filled circle
			if element[0] == 10:
				self.components.bitmapCanvasDisplay.fillColor = "white"
			else:
				self.components.bitmapCanvasDisplay.fillColor = "red"
			self.components.bitmapCanvasDisplay.drawCircle(element,5)

		# Mark each note in the pattern beyond the root
		for digit in Pattern:
			for element in NoteMarkers[(root + int(digit)) % 12]:
				if element[0] == 10:
					self.components.bitmapCanvasDisplay.fillColor = "white"
				else:
					self.components.bitmapCanvasDisplay.fillColor = "red"
				self.components.bitmapCanvasDisplay.drawCircle(element,5)
			root = root + int(digit)
		
	def mark_Fretboard_Chord(self, Strings, NoteMarkers):
	
		# Traverse each string
		for String in Strings:
			# Traverse each fret on the current string
			for i in range(self.Keys.index(String[0]),self.Keys.index(String[0]) + 5):
				# Creat a list of the notes in the current chord
				Notes = self.components.textFieldNotes.text.split(", ")
				# Determine if the note of the current string/fret combination is in the list of notes of the current chord
				if Notes.count(self.Keys[i % 12]) != 0:
					# Traverse the list of note markers for the current note
					for Element in NoteMarkers[i % 12]:
						# Find the marker with the same vertical position
						if Element[1] == String[1]:
							# If the note to be marked is an open string, set the fill color to white
							# Otherwise, set it to red
							if Element[0] == 10:
								self.components.bitmapCanvasDisplay.fillColor = "white"
							else:
								self.components.bitmapCanvasDisplay.fillColor = "red"
							self.components.bitmapCanvasDisplay.foregroundColor = "red"
							# Mark the note
							self.components.bitmapCanvasDisplay.drawCircle(Element,5)
					# Break out of the loop because the first note used on the string has been found
					break
						
# Class definition for help window
class scgenHelp(model.Background):
	
	def on_initialize(self, event):
		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.Add(self.components.htmlWindowDisplay, 1, wx.EXPAND)
		sizer.Fit(self)
		sizer.SetSizeHints(self)
		self.panel.SetSizer(sizer)
		self.panel.SetAutoLayout(1)
		self.panel.Layout()


# Instantiate the main window
if __name__ == '__main__':
    app = model.Application(scgenMain, None, rsrcMain)
    app.MainLoop()
