#!/usr/bin/env python

from distutils.core import setup
import py2exe
      
setup(windows=[dict(
            script = "scgen.py",
            icon_resources=[(1, 'scgen.ico')],
            )],
		options = {"py2exe": {"packages": ["encodings"]}},
		data_files=[(".",
                   ["mainWindow.jpg", 
				   "Staff.bmp",
				   "scgen.ico",
				   "scgenHelp.html",
				   "scgen.db",
				   "msvcrt.dll",
				   "sqlite3.def",
				   "sqlite3.dll"])]
)